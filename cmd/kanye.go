package cmd

import (
	"fmt"
	"log"

	"github.com/spf13/cobra"
	kanye "gitlab.com/emanuelfeijo93/Today/service/kanye"
)

func init() {
	rootCmd.AddCommand(kanyeQuote)
}

var kanyeQuote = &cobra.Command{
	Use:   "kanye",
	Short: "kanye tweet",
	Long:  `kanye tweet, some Yeezus knowledge...`,
	Run: func(cmd *cobra.Command, args []string) {
		tweet, err := kanye.GetKanyeTweet()
		if err != nil {
			log.Fatalf("Couldn't get Tweet with error: %v! Try again", err)
		}
		fmt.Println(tweet)
	},
}
