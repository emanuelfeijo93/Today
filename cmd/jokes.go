package cmd

import (
	"fmt"
	"log"
	"strings"

	"github.com/spf13/cobra"
	joke "gitlab.com/emanuelfeijo93/Today/service/jokes"
)

func init() {
	rootCmd.AddCommand(jokeCmd)
}

var jokeCmd = &cobra.Command{
	Use:   "joke",
	Short: "a random joke",
	Run: func(cmd *cobra.Command, args []string) {
		j, err := joke.GetJoke()
		if err != nil {
			log.Fatal("Couldn't get joke: ", err)
		}
		jkSplitted := strings.Split(j, "\n")
		for _, jk := range jkSplitted {
			fmt.Println(jk)
		}
	},
}
