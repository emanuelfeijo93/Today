package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
	"gitlab.com/emanuelfeijo93/Today/service/weather"
)

func init() {
	rootCmd.AddCommand(weatherCmd)
}

// <city> <y, n>
var weatherCmd = &cobra.Command{
	Use:   "weather",
	Short: "weather by city name",
	Long: `Giving a city name (first argument), you get the weather for that city.
			then second is get the next five days info or only today.
			all information provided by IPMA"`,
	Run: func(c *cobra.Command, args []string) {
		if len(args) != 2 {
			fmt.Println("bad usage")
			return
		}

		var w string
		var nextFive bool
		if args[1] == "y" {
			nextFive = true
		} else {
			nextFive = false
		}
		w, err := weather.GetWeather(args[0], nextFive)
		if err != nil {
			fmt.Println(err)
			return
		}
		fmt.Println(w)
	},
}
