package cmd

import (
	"fmt"
	"log"

	"github.com/spf13/cobra"
	quotes "gitlab.com/emanuelfeijo93/Today/service/quotes"
)

func init() {
	rootCmd.AddCommand(quotesCmd)
}

var quotesCmd = &cobra.Command{
	Use:   "quote",
	Short: "Inspirational Quote",
	Long: `Everyone needs a little push when times are hard, so when running this command 
		will get a little inspiration to help you.
	`,
	Run: func(cmd *cobra.Command, args []string) {
		quote, err := quotes.GetInspiratonalQuote()
		if err != nil {
			log.Panicf("Couldn't get the quote with error: %v", err)
		}
		fmt.Println(quote)

	},
}
