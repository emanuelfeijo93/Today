package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
)

var rootCmd = &cobra.Command{
	Use:   "Today",
	Short: "Today CLI command",
	Long:  `Today CLI command meets the daily needs for everyone working with a terminal`,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("Welcome to Today CLI Application")
		fmt.Println("Availble subcommands:")
		for i, c := range cmd.Commands() {
			if c.Use != "help [command]" {
				fmt.Printf("\t%v: %v --> %v\n", i, c.Use, c.Short)
			}

		}
	},
}

func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
