package jokes

import (
	"gitlab.com/emanuelfeijo93/Today/service/response"
)

const (
	APIUri = "https://v2.jokeapi.dev/joke/Any"
)

type JokeResponse struct {
	Error      bool        `json:"error"`
	Category   string      `json:"category"`
	TypeOfJoke string      `json:"type"`
	Joke       string      `json:"joke"`
	Setup      string      `json:"setup"`
	Delivery   string      `json:"delivery"`
	Flags      interface{} `json:"flags"`
	Id         int         `json:"id"`
	Safe       bool        `json:"safe"`
	Lang       string      `json:"lang"`
}

func GetJoke() (string, error) {
	var jkRes JokeResponse
	err := response.RequestResponse(APIUri, &jkRes)
	if err != nil {
		return "", err
	}
	if jkRes.TypeOfJoke == "twopart" {
		return jkRes.Setup + "\n" + jkRes.Delivery, nil
	}

	return jkRes.Joke, nil
}
